<?php

namespace tests;

use app\models;

/**
 * UserTest contains test cases for user model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class UserTest extends \Codeception\Test\Unit
{
    private function repo_one()
    {
      return new models\GithubRepo('repo1', 4, 4, 4);
    }

    private function repo_two()
    {
        return new models\GithubRepo('repo2', 2, 2, 2);
    }

    private function repo_three()
    {
        return new models\GithubRepo('repo3', 1, 1, 1);
    }

    private function repos()
    {
        return [$this->repo_one(), $this->repo_two(), $this->repo_three()];
    }

    private function user()
    {
        return new models\User('99', 'user', 'github');
    }

    /**
     * Test case for adding repo models to user model
     * 
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @return void
     */
    public function testAddingRepos()
    {
        $user = $this->user();
        $user->addRepos($this->repos());

        $data = $user->getData();
        $this-> assertTrue($data['repo'][0]['name'] == $this->repo_one()->getName());
        $this-> assertTrue($data['repo'][1]['name'] == $this->repo_two()->getName());
        $this-> assertTrue($data['repo'][2]['name'] == $this->repo_three()->getName());

        try {
            $user->addRepos([1]);
        }
        catch (\Exception $e) {
            $this->assertTrue((get_class($e) == 'LogicException'));
        }
    }

    /**
     * Test case for counting total user rating
     *
     * @return void
     */
    public function testTotalRatingCount()
    {
        $user = $this->user();
        $user->addRepos($this->repos());

        $real_rating = $user->getTotalRating();
        $expected_rating = $this->repo_one()->getRating()+$this->repo_two()->getRating()+$this->repo_three()->getRating();

        $this->assertEquals($expected_rating, $real_rating);
    }

    /**
     * Test case for user model data serialization
     *
     * @return void
     */
    public function testData()
    {
        $user = $this->user();
        $user->addRepos($this->repos());

        $real_data = $user->getData();
        $expected_data = [
            'name' => $user->getName(),
            'platform' => $user->getPlatform(),
            'total-rating' => $user->getTotalRating(),
            #I guess there is a mistake in User->getData. Repo->getData output should be added to 'repos', not to 'repo'.
            'repos' => [],
        ];
        foreach ($this->repos() as $repository) {
            $expected_data['repo'][] = $repository->getData();
        }

        $this->assertEquals($expected_data, $real_data);
    }

    /**
     * Test case for user model __toString verification
     *
     * @return void
     */
    public function testStringify()
    {
        $user = $this->user();
        $user->addRepos($this->repos());

        $real_user_as_string = $user->__toString();
        $expected_user_as_string = sprintf(
            "%-75s %19d 🏆\n%'=98s\n",
            $user->getFullName(),
            $user->getTotalRating(),
            ""
        );
        foreach ($this->repos() as $repository) {
            $expected_user_as_string .= (string)$repository . "\n";
        }

        $this->assertEquals($expected_user_as_string, $real_user_as_string);
    }
}