<?php

/**
 * Base contains test cases for testing api endpoint
 * 
 * @see https://codeception.com/docs/modules/Yii2
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BaseCest
{
    private $first_user = 'abashind';
    private $second_user = 'kfr';
    private $fake_user = 'abashinddd';

    private $platforms = [
        'github',
        'gitlab',
        'bitbucket'];

    private $fake_platform = 'githu';
    
    /**
     * Example test case
     *
     * @return void
     */
    public function cestExample(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'kfr',
            ],
            'platforms' => [
                "{$this->platforms[0]}",
            ]
        ]);
        $expected = json_decode('[
            {
                "name": "kfr",
                "platform": "github",
                "total-rating": 1.5,
                "repos": [],
                "repo": [
                    {
                        "name": "kf-cli",
                        "fork-count": 0,
                        "start-count": 2,
                        "watcher-count": 2,
                        "rating": 1
                    },
                    {
                        "name": "cards",
                        "fork-count": 0,
                        "start-count": 0,
                        "watcher-count": 0,
                        "rating": 0
                    },
                    {
                        "name": "UdaciCards",
                        "fork-count": 0,
                        "start-count": 0,
                        "watcher-count": 0,
                        "rating": 0
                    },
                    {
                        "name": "unikgen",
                        "fork-count": 0,
                        "start-count": 1,
                        "watcher-count": 1,
                        "rating": 0.5
                    }
                ]
            }
        ]');
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with bad request params
     *
     * @return void
     */
    public function cestBadParams(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'user' => [
                $this->first_user
            ],
            'platform' => [
                $this->platforms[0]
            ]
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::BAD_REQUEST);
        $I->see('Missing required parameters: users, platforms');
    }

    /**
     * Test case for api with empty user list
     *
     * @return void
     */
    public function cestEmptyUsers(\FunctionalTester $I)
    {
        $I->amOnPage("/base/api?users[]=[]&platforms[]={$this->platforms[1]}");
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals('[]', $I->grabPageSource());
    }

    /**
     * Test case for api with empty platform list
     *
     * @return void
     */
    public function cestEmptyPlatforms(\FunctionalTester $I)
    {
        try {
            $I->amOnPage("/base/api?users[]={$this->first_user}&platforms[]=[]");
            $I->assertTrue(false);
        }
        catch (Exception $e)
        {
            $I->assertTrue((get_class($e) == 'LogicException'));
        }
    }

    /**
     * Test case for api with non empty platform list
     *
     * @return void
     */
    public function cestSeveralPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage("/base/api?users[]={$this->first_user}&platforms[]={$this->platforms[0]}&platforms[]={$this->platforms[1]}");
        $response = json_decode($I->grabPageSource());
        $I->assertTrue($response[0]->{'platform'} == $this->platforms[0]);
        $I->assertTrue($response[1]->{'platform'} == $this->platforms[1]);
    }

    /**
     * Test case for api with non empty user list
     *
     * @return void
     */
    public function cestSeveralUsers(\FunctionalTester $I)
    {
        $I->amOnPage("/base/api?users[]={$this->first_user}&users[]={$this->second_user}&platforms[]={$this->platforms[0]}");
        $response = json_decode($I->grabPageSource());
        $I->assertTrue($response[0]->{'name'} == $this->first_user);
        $I->assertTrue($response[1]->{'name'} == $this->second_user);
    }

    /**
     * Test case for api with unknown platform in list
     *
     * @return void
     */
    public function cestUnknownPlatforms(\FunctionalTester $I)
    {
        try {
            $I->amOnPage("/base/api?users[]={{$this->first_user}}&platforms[]={$this->fake_platform}");
            $I->assertTrue(false);
        }
        catch (Exception $e)
        {
            $I->assertTrue((get_class($e) == 'LogicException'));
        }
    }

    /**
     * Test case for api with unknown user in list
     *
     * @return void
     */
    public function cestUnknownUsers(\FunctionalTester $I)
    {
        $I->amOnPage("/base/api?users[]={$this->fake_user}}&platforms[]={$this->platforms[0]}");
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->assertEquals('[]', $I->grabPageSource());
    }

    /**
     * Test case for api with mixed (unknown, real) users and non empty platform list
     *
     * @return void
     */
    public function cestMixedUsers(\FunctionalTester $I)
    {
        $I->amOnPage("/base/api?users[]={$this->first_user}&users[]={$this->fake_user}&platforms[]={$this->platforms[0]}");
        $response = json_decode($I->grabPageSource());
        $I->assertTrue(count($response) == 1);
        $I->assertTrue($response[0]->{'name'} == $this->first_user);
    }

    /**
     * Test case for api with mixed (github, gitlab, bitbucket) platforms and non empty user list
     *
     * @return void
     */
    public function cestMixedPlatforms(\FunctionalTester $I)
    {
        #I see no difference between this test and cestSeveralPlatforms.
        #May it be that you meant do request with real and fake platform?
        #if it so, I could implement it. It's not difficult.
        #And it looks like Bitbucket->findUserInfo doesn't work proper. Thus, we have to create bug report in such situation.

        $I->amOnPage("/base/api?users[]={$this->first_user}&platforms[]={$this->platforms[0]}&platforms[]={$this->platforms[1]}&platforms[]={$this->platforms[2]}");
        $response = json_decode($I->grabPageSource());
        $I->assertTrue(count($response) == count($this->platforms));
        $I->assertTrue($response[0]->{'platform'} == $this->platforms[0]);
        $I->assertTrue($response[1]->{'platform'} == $this->platforms[1]);
        $I->assertTrue($response[2]->{'platform'} == $this->platforms[2]);
    }
}