<?php

namespace tests;

use app\components\Factory;

/**
 * FactoryTest contains test cases for factory component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class FactoryTest extends \Codeception\Test\Unit
{
    /**
     * Test case for creating platform component
     * 
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @return void
     */
    public function testCreate()
    {
        $platforms = [
        Factory::PLATFORM_GITHUB, Factory::PLATFORM_GITLAB, Factory::PLATFORM_BITBUCKET
        ];

        $fake_platform = 'githu';

        foreach ($platforms as $platformName)
        {
            $platform = \Yii::$app->factory->create($platformName);
            $this->assertTrue($platform != NULL);
        }

        try
        {
            \Yii::$app->factory->create($fake_platform);
            $this->assertTrue(false);
        }
        catch(\Exception $e)
        {
            $this->assertTrue((get_class($e) == 'LogicException'));
        }

    }
}