<?php

namespace tests;

use app\components;

/**
 * SearcherTest contains test cases for searcher component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class SearcherTest extends \Codeception\Test\Unit
{
    private $platforms_names = [
        'github',
        'gitlab',
        'bitbucket'];

    private function getPlatforms()
    {
        $platforms = [];
        foreach ($this->platforms_names as $platform_name) {
            $platforms[] = \Yii::$app->factory->create($platform_name);
        }
        return $platforms;
    }

    /**
     * Test case for searching via several platforms
     * 
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @return void
     */
    public function testSearcher()
    {
        $correctUserName = 'abashinddd';
        $realUserName = 'abashind';

        $users = \Yii::$app->searcher->search($this->getPlatforms(), [$correctUserName]);
        $this->assertTrue(count($users) == 0);

        $users = \Yii::$app->searcher->search($this->getPlatforms(), [$realUserName]);
        $this->assertEquals(count($this->platforms_names), count($users));
    }
}